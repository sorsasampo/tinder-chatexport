#!/usr/bin/env python3
# Copyright (C) 2018 Sampo Sorsa <sorsasampo@protonmail.com>
import argparse
import json
import logging

import requests

logging.basicConfig(level=logging.DEBUG)

API = 'https://api.gotinder.com/v2/'
API_MATCHES = API + 'matches/'
HEADERS = {
    'accept': 'application/json',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0',
    'x-supported-image-formats': 'jpeg',
    'referer': 'https://api.gotinder.com/',
    'platform': 'web',
    'origin': 'https://tinder.com',
    'app-version': '1020307',
}

parser = argparse.ArgumentParser()
parser.add_argument('-o', '--output', default='messages.json', help='output filename to write to')
parser.add_argument('--match', required=True, help='Match id, 48 character hex string')
parser.add_argument('--page-token', required=True, help='First page_token from /matches/<id>/messages request')
parser.add_argument('--token', required=True, help='x-auth-token header')


def run(args):
    url = API_MATCHES + args.match + '/messages'
    logging.debug('URL: %s' % url)
    headers = {**HEADERS, 'x-auth-token': args.token}
    messages = []
    PARAMS = {
        'count': 100,
        'locale': 'en',
    }

    next_page_token = args.page_token
    while True:
        params = {**PARAMS, 'page_token': next_page_token}

        r = requests.get(url, headers=headers, params=params)
        newMessages = r.json()['data']['messages']
        messages += r.json()['data']['messages']
        logging.debug('Got %d messages, total %d' % (len(newMessages), len(messages)))

        if 'next_page_token' in r.json()['data']:
            next_page_token = r.json()['data']['next_page_token']
        else:
            break

    messages.reverse()
    save_messages(messages, args.output)


def save_messages(messages, filename):
    logging.info('Saving %d messages to file "%s"' % (len(messages), filename))
    with open(filename, 'w') as f:
        json.dump(messages, f, sort_keys=True)


if __name__ == '__main__':
    args = parser.parse_args()
    logging.debug(args)
    run(args)
